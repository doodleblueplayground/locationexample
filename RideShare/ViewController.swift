//
//  ViewController.swift
//  RideShare
//
//  Created by Goutham on 01/08/16.
//  Copyright © 2016 doodleblue. All rights reserved.
//

import UIKit
import GoogleMaps

class ViewController: UIViewController, CLLocationManagerDelegate, GMSMapViewDelegate {
    
    @IBOutlet var mapView: GMSMapView!
    var locManager: CLLocationManager!
    var isFirst = false

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        //sdghfdsadfasdfasdgafsds
        // jasdhasjdgsadhafsdhs
        /// ahsdgashjsghjgsdas
        
        let camera = GMSCameraPosition.cameraWithLatitude(28.61, longitude: 77.29, zoom: 10.0)
        self.mapView.camera = camera;
        self.mapView.myLocationEnabled = true
        self.mapView.mapType = kGMSTypeNormal
        
        self.locManager = CLLocationManager()
        self.locManager.delegate = self
        self.locManager.desiredAccuracy = kCLLocationAccuracyBest
        self.locManager.requestAlwaysAuthorization()
        if #available(iOS 9.0, *){
            self.locManager.allowsBackgroundLocationUpdates = true
        }
        self.locManager.startUpdatingLocation()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: Map Delegate Methods
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
    {
        let locationArray = locations as NSArray
        let locationObj = locationArray.lastObject as! CLLocation
        let coord = locationObj.coordinate
        print(coord)
        let path = GMSMutablePath()
        if isFirst == false
        {
            isFirst = true
            mapView.camera = GMSCameraPosition.cameraWithTarget(coord, zoom: 17)
            mapView.delegate = self
            path.addCoordinate(coord)
        }
        path.addCoordinate(coord)
        let polyline = GMSPolyline(path: path)
        polyline.strokeColor = UIColor.blueColor()
        polyline.strokeWidth = 5.0
        polyline.map = mapView
    }
    
    func locationManager(manager: CLLocationManager, didFailWithError error: NSError)
    {
       // if StrandDAppHelper.hasInternetConnection() == true
      //  {
        let alert = UIAlertController(title: "RideShare", message: "RideShare is not able to accessing your current location. If you want to access your current location, go to Settings -> RideShare -> Location -> While Using the App.", preferredStyle: .Alert)
        let okAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default) {
            UIAlertAction in
        }
        alert.addAction(okAction)
        self.presentViewController(alert, animated: true, completion: nil)
        
        let locationCoord:CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: 28.61,longitude: 77.29)
        mapView.camera = GMSCameraPosition.cameraWithTarget(locationCoord, zoom: 17)
        mapView.delegate = self
        locManager.stopUpdatingLocation()
        locManager.delegate = nil
        locManager = nil
    }


}

